# Note for older GNOME versions

We currently support for all (possible) GNOME Versions. Unfortunately, we
removed the older GNOME versions support and it only have a single version
(Version 4.0) with these reasons:

* It doesn't handle the deprecated features: e.g.: shell element actor
  (panel.actor)
* Some newer libraries doesn't work with older versions and vice versa
* Some newer mehthod works with some errors that won't possible for another
  GNOME versions.

But, you can still use older version altough many newer updates won't available.
You can check older commits or branches to see older history even with older
versions support. You can also fork this extension to get evolved.

<small>Last updated: Sunday, June 12 2021 06:52 UTC</small>