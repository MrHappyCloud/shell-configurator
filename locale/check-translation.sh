#!/bin/bash

for file in locale/*.po; do
    if grep --silent "#, fuzzy" "$file"; then
        fuzzy+=("$(basename "$file" .po)")
    fi
done

if [[ -v fuzzy ]]; then
    echo "Shell Configurator [WARNING]: Translations have unclear strings and need an update: ${fuzzy[*]}"
fi