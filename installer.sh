set -e

echo "Shell Configurator installer"
echo "Installer for Shell Configurator extension"
echo

getHelp() {
    echo "Available flags:"
    echo "  --build                 Build extension and create the zip file"
    echo "  --install [TYPE]        Build and install extension to your system"
    echo "  --help                  Show help for flags"
    echo
    echo "install type option:"
    echo "  USER                    Install extension as user ({username}/.local/usr/share/gnome-shell/extensions)"
    echo "  SYSTEM                  Install extension as system (/usr/share/gnome-shell/extensions) - Requires Root"
}

# Declare GNOME Shell Versions
GS_MAJOR_VERSION=$(gnome-shell --version | cut -d' ' -f3 | cut -d'.' -f1)
GS_MINOR_VERSION=$(gnome-shell --version | cut -d' ' -f3 | cut -d'.' -f2)

# change directory to this repo root
cd "$(git rev-parse --show-toplevel)" > /dev/null 2>&1

case $1 in
    --build)
        if [ ! -d "./build" ]; then
            echo "Creating build directory"
            mkdir -p "./build"
        fi

        echo "Building extension..."
        cp -Rv src/* ./build
        cp -v CHANGELOG.md ./build
        cp -v LICENSE ./build

        # Package the extension
        echo "Packing extension to zip..."
        gnome-extensions pack \
            --force \
            --podir="../locale" \
            --extra-source="library" \
            --extra-source="library/modules" \
            --extra-source="schema" \
            --extra-source="ui" \
            --extra-source="widgets" \
            --extra-source="image" \
            --extra-source="presets" \
            --extra-source="suggested-extensions.json" \
            --extra-source="../../CHANGELOG.md" \
            --extra-source="../../LICENSE" \
            './build'

        echo "Done!"
        echo
        exit 0;;

    --install)
        if [ $2 ]; then
            echo "Installing Extension..."
            echo "Checking extension zip file..."
            if [ ! -f 'shell-configurator@adeswanta.shell-extension.zip' ]; then
                echo "[Shell Configurator] [ERROR] shell-configurator@adeswanta.shell-extension.zip file not found."
                echo "[Shell Configurator] [INFO] You need to build package first."
                echo
                exit 1
            fi
            case $2 in
                USER)
                    echo "Install as USER"

                    echo "Installing the extension..."
                    # Install the extension
                    gnome-extensions install --force 'shell-configurator@adeswanta.shell-extension.zip'

                    echo "Changing install-type property on metadata.json..."
                    sed -i 's/system/user/g' "/home/$USER/.local/share/gnome-shell/extensions/shell-configurator@adeswanta/metadata.json"
                    sed -i 's/user/user/g' "/home/$USER/.local/share/gnome-shell/extensions/shell-configurator@adeswanta/metadata.json"

                    echo "Compiling Schemas..."
                    glib-compile-schemas "/home/$USER/.local/share/gnome-shell/extensions/shell-configurator@adeswanta/schema/"

                    echo;;

                SYSTEM)
                    echo "Install as SYSTEM"
                    echo "Checking root privileges..."
                    if [ $(id -u) -ne 0 ]; then
                        echo "[Shell Configurator] [ERROR] You need root privileges to install extension as system"
                        echo
                        exit 1
                    fi

                    if [ ! -d '/usr/share/gnome-shell/extensions/shell-configurator@adeswanta/' ]; then
                        echo "Creating extension folder for '/usr/share/gnome-shell/extensions/'..."
                        mkdir -p '/usr/share/gnome-shell/extensions/shell-configurator@adeswanta/'
                    fi

                    echo "Unzipping extension..."
                    unzip 'shell-configurator@adeswanta.shell-extension.zip' -d '/usr/share/gnome-shell/extensions/shell-configurator@adeswanta/'

                    if [ ! -f '/usr/share/glib-2.0/schemas/org.gnome.shell.extensions.shell-configurator.gschema.xml' ]; then
                        echo "Copying schema to '/usr/share/glib-2.0/schemas/'..."
                        cp '/usr/share/gnome-shell/extensions/shell-configurator@adeswanta/schema/org.gnome.shell.extensions.shell-configurator.gschema.xml' '/usr/share/glib-2.0/schemas/'
                    fi

                    echo "Changing install-type property on metadata.json..."
                    sed -i 's/user/system/g' '/usr/share/gnome-shell/extensions/shell-configurator@adeswanta/metadata.json'
                    sed -i 's/system/system/g' '/usr/share/gnome-shell/extensions/shell-configurator@adeswanta/metadata.json'

                    echo "Compiling Schemas..."
                    glib-compile-schemas '/usr/share/glib-2.0/schemas/'

                    echo;;

                *)
                    echo "[Shell Configurator] [ERROR] Unknown install type"
                    echo "  USER: Install as User"
                    echo "  SYSTEM: Install as system - Requires Root"
                    echo
                    exit 1;;

            esac
        else
            echo "[Shell Configurator] [ERROR] Install type must be specified"
            echo "  USER: Install as User"
            echo "  SYSTEM: Install as system - Requires Root"
            echo
            exit 1
        fi
        echo "Done! Please restart/restore GNOME Shell or log out to apply changes." || \
        { echo "[Shell Configurator] [ERROR] An error occurred while installing extension."; exit 1; }
        echo
        exit 0;;

    --help)
        getHelp
        echo
        exit 0;;

    *)
        echo "[Shell Configurator] [ERROR] Unknown flag or empty."
        echo
        getHelp
        echo
        exit 1;;
esac